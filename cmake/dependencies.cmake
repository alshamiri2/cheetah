include(cmake/thirdparty.cmake)
set(THIRDPARTY_DIR "${CMAKE_SOURCE_DIR}/thirdparty")
set(THIRDPARTY_BINARY_DIR "${CMAKE_BINARY_DIR}/thirdparty")

if(CMAKE_BUILD_TYPE MATCHES documentation)
  # -- only building the documentation, so only doc dependencies required
  find_package(Panda REQUIRED cmake)
  list(APPEND CMAKE_MODULE_PATH "${PANDA_CMAKE_MODULE_PATH}")
  include(cmake/cuda.cmake)

else(CMAKE_BUILD_TYPE MATCHES documentation)
    # thirdparty dependencies
    include(cmake/googletest.cmake)
    include(compiler_settings)
    include(cmake/cuda.cmake)
    include(cmake/opencl.cmake)
    include(cmake/boost.cmake)
    find_package(AstroTypes REQUIRED) # depends on boost
    include_directories(SYSTEM ${ASTROTYPES_INCLUDE_DIR})

    find_package(Panda REQUIRED)
    list(APPEND CMAKE_MODULE_PATH "${PANDA_CMAKE_MODULE_PATH}")
#    message("CMAKE_MODULE_PATH=${CMAKE_MODULE_PATH}")

    find_package(AstroAccelerate)
    find_package(FFTW)
    find_package(PSRDADA)

    include_directories(SYSTEM ${Boost_INCLUDE_DIR}
                               ${PANDA_INCLUDE_DIR})

    if(FFTW_FOUND)
        include_directories(SYSTEM ${FFTW_INCLUDES})
    endif(FFTW_FOUND)
    if(ASTROACCELERATE_FOUND)
        include_directories(SYSTEM ${ASTROACCELERATE_INCLUDE_DIR})
    endif(ASTROACCELERATE_FOUND)
    if (PSRDADA_FOUND)
        include_directories(SYSTEM ${PSRDADA_INCLUDE_DIR})
    endif(PSRDADA_FOUND)
    set(DEPENDENCY_LIBRARIES
        ${PANDA_LIBRARIES}
        #${ASTROTYPES_LIBRARIES}
        ${Boost_LIBRARIES}
        ${FFTW_LIBRARIES}
        ${ASTROACCELERATE_LIBRARIES}
        ${PSRDADA_LIBRARIES}
        ${CUDA_DEPENDENCY_LIBRARIES}
        )

endif(CMAKE_BUILD_TYPE MATCHES documentation)

# -- common depemdemcies
include(doxygen)
