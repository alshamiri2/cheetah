# Find the native ASTROTYPES includes and library
#
#  Can request specific components (default is all components)
#  If all the requested components have been found, ASTROTYPES_FOUND will be set to true
#  Components:
#    cmake - Just the Panda cmake components. e.g. set ASTROTYPES_SOURCE_DIR to point to the source.
#            This will set the following variables:
#               ASTROTYPES_cmake_FOUND - true or false
#               ASTROTYPES_cmake_MODULE_PATH  - directory found containing the astrotypes exported cmake modules
#                                   this could be used e.g.
#                                       list(APPEND cmake_MODULE_PATH ${ASTROTYPES_cmake_MODULE_PATH})
#    libs - The Panda libraries and header files
#            This will set the following variables:
#               ASTROTYPES_LIBRARIES      - List of libraries when using astrotypes.
#               ASTROTYPES_TEST_LIBRARIES - List of astrotypes test support libraries
#               ASTROTYPES_LIBRARY_DIR - directory where astrotypes libraries were found
#               ASTROTYPES_INCLUDE_DIR - directory where astrotypes includes were found
#
#    Example:   find_package(AstroTypes REQUIRED libs) - Would only require AstroTypes libraries to be present
#
#
#  The following variables can be set to explicitly specify the locations of astrotypes components in order of precedence
#  ASTROTYPES_LIBRARY_DIR - explicitly define directory where to find astrotypes libraries
#  ASTROTYPES_INCLUDE_DIR - where to find astrotypes includes
#  ASTROTYPES_MODULES_DIR - where to find astrotypes modules
#  ASTROTYPES_SOURCE_DIR  - Top of the Panda source files directory
#  ASTROTYPES_INSTALL_DIR - Top where astrotypes framwork has been installed (lib and include dirs included)

option(USE_THIRDPARTY_ASTROTYPES "Force the use of the version of astrotypes in the thridparty dir, even if a system install is present" false)

# --- START process user input files and dset defaults -----------------
# If no component explicitly requested, then default to everything
IF(NOT ASTROTYPES_FIND_COMPONENTS)
    SET(ASTROTYPES_FIND_COMPONENTS libs)
ENDIF (NOT ASTROTYPES_FIND_COMPONENTS)


# --- END process user input files and dset defaults -----------------

# -- search for installed libs
macro(SEARCH_ASTROTYPES)
    # -- include files
    IF(ASTROTYPES_INCLUDE_DIR)
        SET(ASTROTYPES_INC_DIR ${ASTROTYPES_INCLUDE_DIR})
        UNSET(ASTROTYPES_INCLUDE_DIR)
    ENDIF (ASTROTYPES_INCLUDE_DIR)
    # -- cmake
    IF (ASTROTYPES_MODULES_DIR)
        SET(ASTROTYPES_MOD_DIR ${ASTROTYPES_MODULES_DIR})
        UNSET(ASTROTYPES_MODULES_DIR)
    ENDIF (ASTROTYPES_MODULES_DIR)

    INCLUDE(FindPackageHandleCompat)
    FOREACH(component ${ASTROTYPES_FIND_COMPONENTS})
        string(TOLOWER ${component} component)
        IF(${component} MATCHES "libs")
            # -- include files
            FIND_PATH(ASTROTYPES_INCLUDE_DIR astrotypes/Version.h
                      PATHS ${ASTROTYPES_INC_DIR}
                            ${ASTROTYPES_INSTALL_DIR}/include
                            /usr/local/include
                            /usr/include )
            message("Found ASTROTYPES_INCLUDE_DIR : ${ASTROTYPES_INCLUDE_DIR}")
            # -- lib files
            SET(ASTROTYPES_NAMES astrotypes)
            FOREACH( lib ${ASTROTYPES_NAMES} )
                FIND_LIBRARY(ASTROTYPES_LIBRARY_${lib}
                             NAMES ${lib}
                             PATHS ${ASTROTYPES_LIBRARY_DIR} ${ASTROTYPES_INSTALL_DIR} ${ASTROTYPES_INSTALL_DIR}/lib /usr/local/lib /usr/lib
                )
                LIST(APPEND ASTROTYPES_LIBRARIES ${ASTROTYPES_LIBRARY_${lib}})
            ENDFOREACH(lib)
            FIND_PACKAGE_HANDLE_STANDARD_ARGS(ASTROTYPES_LIBS DEFAULT_MSG ASTROTYPES_LIBRARIES ASTROTYPES_INCLUDE_DIR)
        ENDIF(${component} MATCHES "libs")
        IF(${component} MATCHES "cmake")
            FIND_PATH(ASTROTYPES_MODULES_DIR astrotypes/cmake/git_version.cmake
                    "${ASTROTYPES_MOD_DIR}"
                    "${ASTROTYPES_SOURCE_DIR}"
                    "${ASTROTYPES_INSTALL_DIR}/share"
                    /usr/local/share
                    /usr/share )

            FIND_PACKAGE_HANDLE_STANDARD_ARGS(ASTROTYPES_MODULES DEFAULT_MSG ASTROTYPES_MODULES_DIR)

            # Append cmake path to ASTROTYPES_MODULE_PATH (It should be last)
            IF(ASTROTYPES_MODULES_FOUND)
                SET(ASTROTYPES_CMAKE_MODULE_PATH "${ASTROTYPES_MODULES_DIR}/astrotypes/cmake")
                SET(ASTROTYPES_CMAKE_FOUND true) # - for dependency checking
            ELSE(ASTROTYPES_MODULES_FOUND)
                SET(ASTROTYPES_CMAKE_FOUND false)
            ENDIF(ASTROTYPES_MODULES_FOUND)
        ENDIF(${component} MATCHES "cmake")
    ENDFOREACH(component)
endmacro(SEARCH_ASTROTYPES)

# Check if the user wants to use the included astrotypes, regardless of if they have a existing install
if(NOT USE_THIRDPARTY_ASTROTYPES)
    message(STATUS "Checking for an installed astrotypes library...")
    SEARCH_ASTROTYPES()
    # --  check if we need to default to thridparty lib
    if(ASTROTYPES_FOUND)
        message(STATUS "AstroTypes found!")
    else()
        message(STATUS "Installed version of AstroTypes not found! Defaulting to version in thirdparty...")
    endif()
endif()

if(NOT ASTROTYPES_FOUND) # If no astrotypes install is found, use the included third-party astrotypes
    message(STATUS "Using thirdparty astrotypes lib. Building astrotypes ....")
    set(ASTROTYPES_BINARY_DIR "${THIRDPARTY_BINARY_DIR}/astrotypes/src/cpp")
    set(ASTROTYPES_SOURCE_DIR "${THIRDPARTY_DIR}/astrotypes/src/cpp")
    add_thirdparty_subdirectory("pss/astrotypes" "${ASTROTYPES_SOURCE_DIR}")
    set(ASTROTYPES_INCLUDE_DIR "${ASTROTYPES_SOURCE_DIR}" "${ASTROTYPES_BINARY_DIR}")
    set(ASTROTYPES_LIBRARY_DIR "${ASTROTYPES_BINARY_DIR}/astrotypes")
    set(ASTROTYPES_LIBRARY_astrotypes "${ASTROTYPES_LIBRARY_DIR}/libastrotypes.a")
    #set(ASTROTYPES_LIBRARIES "${ASTROTYPES_LIBRARY_DIR}/libastrotypes.a")
    SET(ASTROTYPES_CMAKE_FOUND true) # - for dependency checking
endif()


# Handle components
# I HAVE to supply a required variable - so give it one. All other required variables are dealt with above.
# This call to FPHSV is just to correctly handle the components
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ASTROTYPES REQUIRED_VARS ASTROTYPES_FOUND HANDLE_COMPONENTS)
