#---------------------------------------------------------
# OPENCL development toolkit and configuration
#
# Interface Variables:
#---------------------------------------------------------
if(ENABLE_OPENCL)
  if(${CMAKE_VERSION_MAJOR} >= 3 AND ${CMAKE_VERSION_MINOR} > 0)
    # use cmakes version if there is one
    find_package(OpenCL REQUIRED)
  else(${CMAKE_VERSION_MAJOR} >= 3 AND ${CMAKE_VERSION_MINOR} > 0)
    # ------------------------------------------------
    # START rough and ready script for older cmake versions
    # ------------------------------------------------
    find_path(OpenCL_INCLUDE_DIR
      NAMES
        CL/cl.h OpenCL/cl.h
      PATHS
        ${OpenCL_INCLUDE_DIR}
        ${OpenCL_INSTALL_DIR}
        ENV "PROGRAMFILES(X86)"
        ENV AMDAPPSDKROOT
        ENV INTELOCLSDKROOT
        ENV NVSDKCOMPUTE_ROOT
        ENV CUDA_PATH
        ENV ATISTREAMSDKROOT
        /usr/local
        /usr
      PATH_SUFFIXES
        include
        OpenCL/common/inc
        "AMD APP/include"
    )
    find_library(OpenCL_LIBRARY
      NAMES OpenCL
      PATHS
        ${OpenCL_LIBRARY_DIR}
        ${OpenCL_INSTALL_DIR}
      PATH_SUFFIXES
        lib
        "AMD APP/lib/x86"
        lib/x86
        lib/Win32
        OpenCL/common/lib/Win32
    )
    set(OpenCL_LIBRARIES ${OpenCL_LIBRARY})
    set(OpenCL_INCLUDE_DIRS ${OpenCL_INCLUDE_DIR})

    INCLUDE(FindPackageHandleCompat)
    FIND_PACKAGE_HANDLE_STANDARD_ARGS(OpenCL DEFAULT_MSG OpenCL_LIBRARIES OpenCL_INCLUDE_DIRS)

    IF(NOT OpenCL_FOUND)
        SET( OpenCL_LIBRARIES )
        SET( OpenCL_TEST_LIBRARIES )
    ENDIF(NOT OpenCL_FOUND)

    MARK_AS_ADVANCED(OpenCL_LIBRARIES OpenCL_TEST_LIBRARIES OpenCL_INCLUDE_DIR)

    # ------------------------------------------------
    # END rough and ready script for older cmake versions
    # ------------------------------------------------
  endif(${CMAKE_VERSION_MAJOR} >= 3 AND ${CMAKE_VERSION_MINOR} > 0)

  include_directories(${OpenCL_INCLUDE_DIRS})

  set(CMAKE_CXX_FLAGS "-DENABLE_OPENCL ${CMAKE_CXX_FLAGS}")
  list(APPEND DEPENDENCY_LIBRARIES ${OpenCL_LIBRARIES})
endif(ENABLE_OPENCL)
