#include "cheetah/fft/cuda/Fft.cuh"

namespace ska {
namespace cheetah {
namespace fft {
namespace cuda {

Fft::Fft(Config const& config, fft::Config const& algo_config)
	: utils::AlgorithmBase<Config, fft::Config>(config,algo_config)
{
}

} // namespace cuda
} // namespace fft
} // namespace cheetah
} // namespace ska