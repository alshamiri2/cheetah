#include "cheetah/fft/cuda/Fft.cuh"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace fft {
namespace cuda {
namespace detail {

/**
 * @brief      A helper class for selecting the correct execution calls from cuFFT
 *
 * @details    This class is used by the Cuda Fft implementation to provide the
 *             correct FFT execution calls from the cuFFT library based on whether
 *             we are working with float or double data.
 *
 *             Note that calls to cufftExecXXX functions appear to not be compatible
 *             with constant input types (likely as the base implementation is straight
 *             C code without overloading and the library implements inplace transforms)
 *
 * @tparam     T     The base value type of the corresponding transform (float or double)
 */
template <typename T>
struct CufftHelper
{
};

template <>
struct CufftHelper<float>
{
    typedef cufftReal RealType;
    typedef cufftComplex ComplexType;

    static inline cufftResult r2c(cufftHandle plan, RealType const* input, ComplexType* output)
    {
        RealType* input_non_const = const_cast<RealType*>(input);
        return cufftExecR2C(plan, input_non_const, output);
    }

    static inline cufftResult c2r(cufftHandle plan, ComplexType const* input, RealType* output)
    {
        ComplexType* input_non_const = const_cast<ComplexType*>(input);
        return cufftExecC2R(plan, input_non_const, output);
    }

    static inline cufftResult c2c(cufftHandle plan, ComplexType const* input, ComplexType* output, int direction)
    {
        ComplexType* input_non_const = const_cast<ComplexType*>(input);
        return cufftExecC2C(plan, input_non_const, output, direction);
    }
};

template <>
struct CufftHelper<double>
{
    typedef cufftDoubleReal RealType;
    typedef cufftDoubleComplex ComplexType;

    static inline cufftResult r2c(cufftHandle plan, RealType const* input, ComplexType* output)
    {
        RealType* input_non_const = const_cast<RealType*>(input);
        return cufftExecD2Z(plan, input_non_const, output);
    }

    static inline cufftResult c2r(cufftHandle plan, ComplexType const* input, RealType* output)
    {
        ComplexType* input_non_const = const_cast<ComplexType*>(input);
        return cufftExecZ2D(plan, input_non_const, output);
    }

    static inline cufftResult c2c(cufftHandle plan, ComplexType const* input, ComplexType* output, int direction)
    {
        ComplexType* input_non_const = const_cast<ComplexType*>(input);
        return cufftExecZ2Z(plan, input_non_const, output, direction);
    }
};
} // namespace detail


template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& gpu,
    data::TimeSeries<cheetah::Cuda,T,InputAlloc> const& input,
    data::FrequencySeries<cheetah::Cuda, typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,OutputAlloc>& output)
{
    typedef detail::CufftHelper<T> Cufft;
    typedef typename Cufft::RealType RealType;
    typedef typename Cufft::ComplexType ComplexType;
    PANDA_LOG_DEBUG << "GPU ID: "<<gpu.device_id();
    //update the size of the output buffer to match output transform size
    output.resize(input.size()/2 + 1);
    //Calculate the new frequency step that the output will have
    output.frequency_step((1.0f/(input.sampling_interval().value() * input.size())) * data::hz);
    RealType const* in = thrust::raw_pointer_cast(input.data());
    ComplexType* out = (ComplexType*) thrust::raw_pointer_cast(output.data());
    CUFFT_ERROR_CHECK(Cufft::r2c(_plan.plan<T>(R2C,input.size(),1), in, out));
}

template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& gpu,
    data::FrequencySeries<cheetah::Cuda, typename data::ComplexTypeTraits<cheetah::Cuda,T>::type, InputAlloc> const& input,
    data::TimeSeries<cheetah::Cuda,T,OutputAlloc>& output)
{
    typedef detail::CufftHelper<T> Cufft;
    typedef typename Cufft::RealType RealType;
    typedef typename Cufft::ComplexType ComplexType;
    PANDA_LOG_DEBUG << "GPU ID: "<<gpu.device_id();
    //update the size of the output buffer to match output transform size
    output.resize(2*(input.size() - 1));
    //Calculate the new sampling time that the output will have
    output.sampling_interval((1.0f/(input.frequency_step().value()*output.size())) * data::seconds);
    ComplexType const* in = (ComplexType*) thrust::raw_pointer_cast(input.data());
    RealType* out = thrust::raw_pointer_cast(output.data());
    CUFFT_ERROR_CHECK(Cufft::c2r(_plan.plan<T>(C2R,input.size(),1), in, out));
}

template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& gpu,
    data::TimeSeries<cheetah::Cuda, typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,InputAlloc> const& input,
    data::FrequencySeries<cheetah::Cuda, typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,OutputAlloc>& output)
{
    typedef detail::CufftHelper<T> Cufft;
    typedef typename Cufft::ComplexType ComplexType;
    PANDA_LOG_DEBUG << "GPU ID: "<<gpu.device_id();
    //update the size of the output buffer to match output transform size
    output.resize(input.size());
    //Calculate the new frequency step that the output will have
    output.frequency_step((1.0f/(input.sampling_interval().value() * input.size())) * data::hz);
    ComplexType const* in = (ComplexType*) thrust::raw_pointer_cast(input.data());
    ComplexType* out = (ComplexType*) thrust::raw_pointer_cast(output.data());
    CUFFT_ERROR_CHECK(Cufft::c2c(_plan.plan<T>(C2C,input.size(),1), in, out, CUFFT_FORWARD));
}

template <typename T, typename InputAlloc, typename OutputAlloc>
void Fft::process(ResourceType& gpu,
    data::FrequencySeries<cheetah::Cuda, typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,InputAlloc> const& input,
    data::TimeSeries<cheetah::Cuda, typename data::ComplexTypeTraits<cheetah::Cuda,T>::type,OutputAlloc>& output)
{
    typedef detail::CufftHelper<T> Cufft;
    typedef typename Cufft::ComplexType ComplexType;
    PANDA_LOG_DEBUG << "GPU ID: "<<gpu.device_id();
    //update the size of the output buffer to match output transform size
    output.resize(input.size());
    //Calculate the new sampling time that the output will have
    output.sampling_interval((1.0f/(input.frequency_step().value()*output.size())) * data::seconds);
    ComplexType const* in = (ComplexType*) thrust::raw_pointer_cast(input.data());
    ComplexType* out = (ComplexType*) thrust::raw_pointer_cast(output.data());
    CUFFT_ERROR_CHECK(Cufft::c2c(_plan.plan<T>(C2C,input.size(),1), in, out, CUFFT_INVERSE));
}

} // namespace cuda
} // namespace fft
} // namespace cheetah
} // namespace ska