/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FDAS_OPENCL_FDAS_H
#define SKA_CHEETAH_FDAS_OPENCL_FDAS_H

#include "cheetah/utils/Architectures.h"
#include "cheetah/fdas/opencl/Config.h"
#include "cheetah/data/DmTime.h"

namespace ska {
namespace cheetah {
namespace fdas {
namespace opencl {

/**
 * @brief
 *    Interface to the opencl fdas algorithm
 *
 */

class Fdas
{
    public:
        typedef cheetah::Fpga Architecture;

    public:
        Fdas(Config const& config);
        ~Fdas();

        /**
         * @brief task interface to launch the algorithm on the provided device
         */
        void operator()(panda::PoolResource<Fpga>& device, std::shared_ptr<data::DmTime<Cpu>> data);

    private:
};


} // namespace opencl
} // namespace fdas
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_FDAS_OPENCL_FDAS_H 
