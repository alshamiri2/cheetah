/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SPSIFT_SPSIFT_H
#define SKA_CHEETAH_SPSIFT_SPSIFT_H

#include "cheetah/spsift/Config.h"
#include "cheetah/data/SpCcl.h"

namespace ska {
namespace cheetah {
namespace spsift {

/**
 * @brief A module to sift through Single Pulse candidates based on some
 *        threshold in width, DM and S/N.
 */

class SpSift
{
    public:
        SpSift(Config const& config);
        ~SpSift();

        /**
         * @brief  return the Configuration object
         */
        Config const& config() const;

        /**
         * @brief  remove candidates that do not fir the configured criteria
         */
        template<typename NumRep>
        void operator()(data::SpCcl<NumRep>& candidate_list) const;

    private:
        Config const& _config;
};

} // namespace spsift
} // namespace cheetah
} // namespace ska
#include "cheetah/spsift/detail/SpSift.cpp"
#endif // SKA_CHEETAH_SPSIFT_SPSIFT_H
