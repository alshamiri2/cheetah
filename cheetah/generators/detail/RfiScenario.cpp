/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/generators/RfiScenario.h"


namespace ska {
namespace cheetah {
namespace generators {


// -------------------- Scenario 0 ---------------------
template<typename T>
RfiScenario<0, T>::RfiScenario()
{
}

template<typename T>
RfiScenario<0, T>::~RfiScenario()
{
}

template<typename T>
void RfiScenario<0, T>::next(DataType&)
{
}

// -------------------- Scenario 1 ---------------------
template<typename T>
RfiScenario<1, T>::RfiScenario()
{
}

template<typename T>
RfiScenario<1, T>::~RfiScenario()
{
}

template<typename T>
void RfiScenario<1, T>::next(DataType& data)
{
    unsigned factor = 4;
    std::size_t min_channel  = data.number_of_channels()/factor;
    std::size_t max_channel  = (factor - 1) * min_channel;
    std::size_t min_sample   = data.number_of_spectra()/factor;
    std::size_t max_sample   = (factor - 1) * min_sample;

    this->rfi_ramp_block(data, min_channel, max_channel, min_sample, max_sample);
}

// -------------------- Scenario 2 ---------------------
template<typename T>
RfiScenario<2, T>::RfiScenario()
{
}

template<typename T>
RfiScenario<2, T>::~RfiScenario()
{
}

template<typename T>
void RfiScenario<2, T>::next(DataType& data)
{
    unsigned factor = 4;
    std::size_t min_channel  = data.number_of_channels()/factor;
    std::size_t max_channel  = 3 * min_channel;
    std::size_t min_sample   = data.number_of_spectra()/factor;
    std::size_t max_sample   = 3 * min_sample;

    this->rfi_gaussian_block(data, min_channel, max_channel, min_sample, max_sample);
}

// -------------------- Scenario 3 ---------------------
template<typename T>
RfiScenario<3, T>::RfiScenario()
{
}

template<typename T>
RfiScenario<3, T>::~RfiScenario()
{
}

template<typename T>
void RfiScenario<3, T>::next(DataType& data)
{
    unsigned factor                   =  8;
    unsigned channel_stride           =  data.number_of_channels()/factor;
    unsigned sample_stride            =  data.number_of_spectra()/factor;

    std::size_t min_channel           =  1 * channel_stride;
    std::size_t max_channel           =  3 * channel_stride;
    std::size_t min_sample            =  1 * sample_stride;
    std::size_t max_sample            =  3 * sample_stride;

    this->rfi_gaussian_block(data, min_channel, max_channel, min_sample, max_sample);


    min_channel                       =  5 * channel_stride;
    max_channel                       =  7 * channel_stride;
    min_sample                        =  1 * sample_stride;
    max_sample                        =  3 * sample_stride;

    this->rfi_gaussian_block(data, min_channel, max_channel, min_sample, max_sample);


    min_channel                       =  1 * channel_stride;
    max_channel                       =  3 * channel_stride;
    min_sample                        =  5 * sample_stride;
    max_sample                        =  7 * sample_stride;

    this->rfi_gaussian_block(data, min_channel, max_channel, min_sample, max_sample);


    min_channel                       =  5 * channel_stride;
    max_channel                       =  7 * channel_stride;
    min_sample                        =  5 * sample_stride;
    max_sample                        =  7 * sample_stride;

    this->rfi_gaussian_block(data, min_channel, max_channel, min_sample, max_sample);
}

// -------------------- Scenario 4 ---------------------
template<typename T>
RfiScenario<4, T>::RfiScenario()
{
}

template<typename T>
RfiScenario<4, T>::~RfiScenario()
{
}

template<typename T>
void RfiScenario<4, T>::next(DataType& data)
{
    unsigned factor                   =  8;
    unsigned channel_stride           =  data.number_of_channels()/factor;
    unsigned sample_stride            =  data.number_of_spectra()/factor;

    std::size_t min_channel           =  1 * channel_stride;
    std::size_t max_channel           =  6 * channel_stride;
    std::size_t min_sample            =  1 * sample_stride;
    std::size_t max_sample            =  3 * sample_stride;

    this->rfi_gaussian_block(data, min_channel, max_channel, min_sample, max_sample);


    min_channel                       =  5 * channel_stride;
    max_channel                       =  7 * channel_stride;
    min_sample                        =  1 * sample_stride;
    max_sample                        =  6 * sample_stride;

    this->rfi_gaussian_block(data, min_channel, max_channel, min_sample, max_sample);


    min_channel                       =  1 * channel_stride;
    max_channel                       =  6 * channel_stride;
    min_sample                        =  5 * sample_stride;
    max_sample                        =  7 * sample_stride;

    this->rfi_gaussian_block(data, min_channel, max_channel, min_sample, max_sample);


    min_channel                       =  5 * channel_stride;
    max_channel                       =  7 * channel_stride;
    min_sample                        =  5 * sample_stride;
    max_sample                        =  7 * sample_stride;

    this->rfi_gaussian_block(data, min_channel, max_channel, min_sample, max_sample);
}

} // namespace generators
} // namespace cheetah
} // namespace ska
