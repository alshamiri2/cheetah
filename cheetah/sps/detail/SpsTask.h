/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SPS_DETAIL_SPSTASK_H
#define SKA_CHEETAH_SPS_DETAIL_SPSTASK_H

#include "cheetah/sps/astroaccelerate/Sps.h"
#include "cheetah/data/DmTime.h"
#include "cheetah/data/TimeFrequency.h"
#include "panda/AlgorithmTuple.h"
#include "panda/arch/nvidia/Nvidia.h"

namespace ska {
namespace cheetah {
namespace sps {

/**
 * @brief Single pulse search asynchronous task
 *
 * @details
 *
 */
template<typename DmHandler, typename SpHandler, typename BufferType>
class SpsTask
{
    private:
        // to extend to different architectures add the alorithm inside this tuple
        typedef panda::AlgorithmTuple<astroaccelerate::Sps> ImplementationsType;

    public:
        typedef ImplementationsType::Architectures Architectures;

    public:
        SpsTask(sps::Config const& config, DmHandler dm_handler, SpHandler sm_handler);
        SpsTask(SpsTask const&) = delete;
        ~SpsTask();

        /**
         * @brief execute Sps task on a given accelerator
         */
        template<typename Arch>
        void operator()(panda::PoolResource<Arch>&, BufferType buffer);

        /**
         * @brief the number of samples to be copied from the ned of one buffer in tot the beginning of the next
         * @details
         *    tprocessed is the number of time samples required to be able to process the specified dm ranges (via the config)
         */
        template<typename Arch>
        std::size_t buffer_overlap() const;

        /**
         * @brief specify parameters to generate a suitable dedispersion compute strategy
         * @returns the minimum sample size required for the specified parameters
         */
        template<typename... Args>
        std::size_t set_dedispersion_strategy(Args&&...);

        /**
         * @brief call the dm handler directly with the provided data
         */
        template<typename DataType>
        void call_dm_handler(DataType&& d) const;

        /**
         * @brief call the sp handler directly with the provided data
         */
        template<typename DataType>
        void call_sp_handler(DataType&& d) const;

    private:
        ImplementationsType _algos;
        DmHandler _dm_handler;
        SpHandler _sp_handler;
};

} // namespace sps
} // namespace cheetah
} // namespace ska

#include "cheetah/sps/detail/SpsTask.cpp"
#endif // SKA_CHEETAH_SPS_DETAIL_SPSTASK_H
