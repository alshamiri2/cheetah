#include "cheetah/ddtr/DedispersionConfig.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/sps/test_utils/SpsTester.h"
#include <memory>
#include <vector>


namespace ska {
namespace cheetah {
namespace sps {
namespace astroaccelerate {
namespace test {

struct AstroAccelerateTraits : public sps::test::SpsTesterTraits<sps::astroaccelerate::Sps::Architecture,sps::astroaccelerate::Sps::ArchitectureCapability>
{
    typedef sps::test::SpsTesterTraits<sps::astroaccelerate::Sps::Architecture, typename sps::astroaccelerate::Sps::ArchitectureCapability> BaseT;
    typedef typename BaseT::Arch Arch;
    void configure(sps::Config& config) override {
        std::unique_ptr<ddtr::DedispersionConfig> dd_config_ptr(new ddtr::DedispersionConfig());
        _configs.emplace_back(std::move(dd_config_ptr));
        ddtr::DedispersionConfig& dd_config = *_configs.back();
        dd_config.dm_start(ddtr::DedispersionConfig::Dm(0.0 * data::parsec_per_cube_cm));
        dd_config.dm_end(ddtr::DedispersionConfig::Dm(60.0 * data::parsec_per_cube_cm));
        dd_config.dm_step(ddtr::DedispersionConfig::Dm(10.0 * data::parsec_per_cube_cm));
        config.add(dd_config);
    }
    private:
        std::vector<std::unique_ptr<ddtr::DedispersionConfig>> _configs; // keep configs in scope
};

} // namespace test
} // namespace astroaccelerate
} // namespace sps
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace sps {
namespace test {

typedef ::testing::Types<sps::astroaccelerate::test::AstroAccelerateTraits> AstroAccelerateTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, SpsTester, AstroAccelerateTraitsTypes);

} // namespace test
} // namespace sps
} // namespace cheetah
} // namespace ska
