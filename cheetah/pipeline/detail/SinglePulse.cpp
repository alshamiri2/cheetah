/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipeline/SinglePulse.h"

namespace ska {
namespace cheetah {
namespace pipeline {


template<typename NumericalT>
SinglePulse<NumericalT>::SinglePulse(CheetahConfig<NumericalT> const& config, BeamConfig const& beam_config)
    : BaseT(config, beam_config)
    , _dedispersion_handler(*this)
    , _sps_handler(*this)
    , _sps(config.sps_config(), _dedispersion_handler, _sps_handler)
    , _spsifter(config.spsift_config())
{
}

template<typename NumericalT>
SinglePulse<NumericalT>::SinglePulse(SinglePulse&& rp)
    : _sps(std::move(rp._sps))
{
}

template<typename NumericalT>
SinglePulse<NumericalT>::~SinglePulse()
{
}

template<typename NumericalT>
void SinglePulse<NumericalT>::operator()(TimeFrequencyType& data)
{
    BaseT::operator()(data); // apply RFI mitigation
    _sps(data);
}

template<typename NumericalT>
SinglePulse<NumericalT>::DedispersionHandler::DedispersionHandler(SinglePulse<NumericalT>& p)
    : _pipeline(p)
{
}

template<typename NumericalT>
void SinglePulse<NumericalT>::DedispersionHandler::operator()(std::shared_ptr<DmTrialType> const&) const
{
}

template<typename NumericalT>
SinglePulse<NumericalT>::SpsHandler::SpsHandler(SinglePulse<NumericalT>& p)
    : _pipeline(p)
{
}

template<typename NumericalT>
void SinglePulse<NumericalT>::SpsHandler::operator()(std::shared_ptr<SpType> const& data) const
{
    _pipeline._spsifter(*data);
    _pipeline.out().send(ska::panda::ChannelId("sps_events"), *data);
}

} // namespace pipeline
} // namespace cheetah
} // namespace ska
