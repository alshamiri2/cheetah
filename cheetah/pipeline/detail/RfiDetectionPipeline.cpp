/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipeline/RfiDetectionPipeline.h"
#include "cheetah/pipeline/CheetahConfig.h"


namespace ska {
namespace cheetah {
namespace pipeline {


template<typename NumericalT>
RfiDetectionPipeline<NumericalT>::RfiDetectionPipeline(CheetahConfig<NumericalT> const& config, BeamConfig const& beam_config)
    : BaseT(config, beam_config)
    , _channel_mask(config.channel_mask_config())
    , _rfim_handler(*this)
    , _rfim(config.rfim_config(), _rfim_handler)
{
}

template<typename NumericalT>
RfiDetectionPipeline<NumericalT>::~RfiDetectionPipeline()
{
}

template<typename NumericalT>
void RfiDetectionPipeline<NumericalT>::operator()(TimeFrequencyType& data)
{
    _channel_mask(data);
    _rfim.run(data);
    this->out().send(panda::ChannelId("rfim"), data);
}

template<typename NumericalT>
RfiDetectionPipeline<NumericalT>::RfiOutputHandler::RfiOutputHandler(RfiDetectionPipeline& pipeline)
    : _pipeline(pipeline)
{
}

template<typename NumericalT>
void RfiDetectionPipeline<NumericalT>::RfiOutputHandler::operator()(TimeFrequencyType& data)
{
    _pipeline.out().send(panda::ChannelId("rfim"), data);
}

} // namespace pipeline
} // namespace cheetah
} // namespace ska
