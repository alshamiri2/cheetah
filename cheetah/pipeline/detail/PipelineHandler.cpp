/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/pipeline/PipelineHandler.h"
#include "cheetah/pipeline/CheetahConfig.h"
#include "cheetah/pipeline/BeamConfig.h"
#include "panda/Abort.h"


namespace ska {
namespace cheetah {
namespace pipeline {

template<typename NumericalRep>
PipelineHandler<NumericalRep>::PipelineHandler(CheetahConfig<NumericalRep> const& config, BeamConfig const& beam_config)
    : _config(config)
    , _out(beam_config.data_config().data_exporter())
{
}

template<typename NumericalRep>
PipelineHandler<NumericalRep>::~PipelineHandler()
{
    _config.pool_manager().wait();
}

template<typename NumericalRep>
void PipelineHandler<NumericalRep>::halt()
{
    throw panda::Abort();
}

} // namespace pipeline
} // namespace cheetah
} // namespace ska
