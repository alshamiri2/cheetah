#ifndef SKA_CHEETAH_DATA_CUDA_SERIES_H
#define SKA_CHEETAH_DATA_CUDA_SERIES_H
#ifdef ENABLE_CUDA
#include "cheetah/data/Series.h"
#include "cheetah/cuda_utils/cuda_thrust.h"

namespace ska {
namespace cheetah {
namespace data {

template <typename ValueType, typename Alloc>
class Series<cheetah::Cuda, ValueType, Alloc>: public VectorLike<thrust::device_vector<ValueType,Alloc>>
{
    public:
        typedef cheetah::Cuda ArchitectureType;
};

} // namespace data
} // namespace cheetah
} // namespace ska

#endif //ENABLE_CUDA
#endif // SKA_CHEETAH_DATA_CUDA_SERIES_H
