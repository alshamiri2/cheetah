#include "cheetah/data/test_utils/TimeSeriesTest.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace data {
namespace test {

typedef ::testing::Types<
    TimeSeriesTestTraits<cheetah::Cuda,float>,
    TimeSeriesTestTraits<cheetah::Cuda,double>,
    TimeSeriesTestTraits<cheetah::Cuda,char>
    > CudaTimeSeriesTestTypes;
TYPED_TEST_CASE(TimeSeriesTest, CudaTimeSeriesTestTypes);

TYPED_TEST(TimeSeriesTest, test_samples)
{
    SampleCountTest<TypeParam>::test(1000L);
    SampleCountTest<TypeParam>::test(1<<23);
}

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
