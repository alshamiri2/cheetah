#include "cheetah/data/test_utils/DmTrialsTest.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace data {
namespace test {

typedef ::testing::Types<
    DmTrialsTestTraits<cheetah::Cpu,float>,
    DmTrialsTestTraits<cheetah::Cpu,double>,
    DmTrialsTestTraits<cheetah::Cpu,char>
    > DmTrialsTestTypes;
TYPED_TEST_CASE(DmTrialsTest, DmTrialsTestTypes);

TYPED_TEST(DmTrialsTest, size_test)
{
    Tester<TypeParam>::size_test();
    Tester<TypeParam>::iterator_test();
}

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska
