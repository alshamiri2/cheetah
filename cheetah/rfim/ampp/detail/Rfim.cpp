/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/ampp/Rfim.h"
#include "cheetah/utils/BinMap.h"
#include "cheetah/data/Units.h"
#include "panda/Error.h"
#include "panda/Log.h"
#include <cmath>
#include <numeric>
#include <type_traits>

namespace ska {
namespace cheetah {
namespace rfim {
namespace ampp {


template<typename NumericT>
cheetah::data::TimeFrequency<Cpu, NumericT>& Rfim::operator()(panda::PoolResource<Cpu>& , std::shared_ptr<cheetah::data::TimeFrequency<Cpu, NumericT>> data) {
    run(*data);
    return *data;
}

// RFI Policies
// describes what  to do when RFI is found
struct InlineReplacePolicy {
    template<typename SampleType>
    static
    void clip_sample(SampleType& sample) {
        std::fill(sample.begin(), sample.end(), 0);
    }

    template<typename SampleType, typename IteratorType>
    static
    void replace_sample(SampleType& sample, IteratorType it) {
        auto begin = sample.begin();
        while (begin != sample.end()){
            *begin = *it;
            ++it;
            ++begin;
        }
    }

    template<typename ChannelType>
    static inline void clip_channel(ChannelType& channel_value)
    {
        channel_value = 0.0;
    }
    
    template<typename ChannelType, typename BandPassData>
    static inline void adjust_channel(ChannelType& channel_value, BandPassData value) {
        channel_value -= value;
    }
    
    template<typename ChannelType>
    static inline void replace_channel(ChannelType& channel_value, ChannelType const& replacement_value) {
        channel_value = replacement_value;
    }
    
    template<typename ChannelType, typename Data>
    static inline void divide_channel(ChannelType& channel_value, Data value) {
        channel_value /= value;
    }
};

struct NullPolicy {
    template<typename SampleType>
    static void clip_sample(SampleType&) {
    }

    template<typename SampleType, typename IteratorType>
    static
    void replace_sample(SampleType&, IteratorType) {
    }

    template<typename ChannelType>
    static void clip_channel(ChannelType&) {
    }

    template<typename ChannelType, typename BandPassData>
    static void adjust_channel(ChannelType& , BandPassData) {
    }

    template<typename ChannelType>
    static inline void replace_channel(ChannelType& , ChannelType const& ) {
    }

    template<typename ChannelType, typename Data>
    static void divide_channel(ChannelType&, Data) {
    }
};

// Use the policy
template<typename RfiPolicy, typename DataType>
void Rfim::run_impl(DataType& data)
{
    unsigned nsamples = data.number_of_spectra();
    unsigned nchannels = data.number_of_channels();
    // degrees of freedom of chi-squared data
    float degrees_of_freedom = 4; 
    // minimum value of chi-squared data compared to the mean
    float mean_minus_minimum = degrees_of_freedom 
      / sqrt(2.0*degrees_of_freedom); 
    float spectrum_rms = 0.; // to avoid an error if CUDA is disabled
    float data_model  = 0.;

    if( data.number_of_channels() == 0 ) return;

    if (_replacement_sample.size() != nchannels) _replacement_sample.resize(nchannels);

    // -- rebin our bandpass to match the incomming data binning
    // -- IMORTANT : assumes equal bin widths
    utils::BinMap<typename DataType::FrequencyType> map( nchannels
            , data.channel_frequencies()[0]
            , data.channel_frequencies().back() );
    // wait for the bandpass lock before we can continue
    std::unique_lock<std::mutex> bandpass_lock(_mutex);
    
    assert(_bandpass.is_valid());
    _bandpass.rebin(map);
    assert(map.number_of_bins()==nchannels);
    double sqr_nchannels = std::sqrt(nchannels);
    for(unsigned sample_number = 0U; sample_number < nsamples; ++sample_number) {
        auto const& bandpass = _bandpass.current_set();
        float spectrum_sum = 0.0;
        float spectrum_sum_sq = 0.0;
        // Split the spectrum into nbands bands for the purpose of matching
        // the spectrum to the model
        unsigned nbands = 8;
        unsigned channels_per_band = nchannels / nbands;
        if(channels_per_band * nbands != nchannels) ++nbands; // add a band for any left over channels
        std::vector<float> good_channels(nbands,0.0);

        // find the minima of the data in each band, and the minima of
        // the model and compare
        std::vector<float> mini_data(nbands,1e6);
        std::vector<float> mini_model(nbands,1e6);
        std::vector<float> data_minus_model(nbands);
        std::vector<float> band_sigma(nbands);
        std::vector<float> band_mean(nbands,0.0);
        std::vector<float> band_mean_sq(nbands,0.0);

        // Find the data minima and model minima in each band

        // Let us also estimate sigma in each band

        // Try this over an adapting stage, lasting as long as the
        // running average buffers
        auto sample = data.spectrum(sample_number);
        if (!_rms_buffer.full())
        {
            unsigned current_channel = 0;
            for(auto const& channel_value : sample) {
                unsigned band;
                if(channels_per_band) {
                    band = (int)current_channel / channels_per_band;
                }
                else {
                    band = 0;
                }
                if (channel_value < mini_data[band]) mini_data[band] = channel_value;
                assert(band < nbands);
                band_mean[band] += channel_value;
                band_mean_sq[band] += std::pow(channel_value,2);
                ++current_channel;
            }
            current_channel = 0;
            for(auto const& bandpass_value : bandpass) {
                unsigned band;
                if(channels_per_band) { 
                    band = (int)current_channel / channels_per_band;
                } else {
                    band = 0;
                }
                assert(band < nbands);
                if (bandpass_value < mini_model[band]) 
                    mini_model[band] = bandpass_value;
                ++current_channel;
            }
            
            // Now find the distances between data and model and the RMSs in
            // each band
            
            for (unsigned b = 0; b < nbands; ++b){
                data_minus_model[b] = mini_data[b] - mini_model[b];
                band_sigma[b] = sqrt(band_mean_sq[b]/channels_per_band 
                                     - std::pow(band_mean[b]/channels_per_band,2)); // TODO adjust for final band size
            }
            // Assume the minimum bandSigma to be the best estimate of this
            // spectrum RMS
            spectrum_rms = *std::min_element(band_sigma.begin(), band_sigma.end());
            
            // Take the median of dataMinusModel to determine the distance
            // from the model
            std::nth_element(data_minus_model.begin(), 
                             data_minus_model.begin()+data_minus_model.size()/2, 
                             data_minus_model.end());
            data_model = (float)*(data_minus_model.begin()+data_minus_model.size()/2);
            
            // since we have used the minima to determine this
            // distance, we assume that data_model is actually k/sqrt(k)
            // sigma away from the real value, where k is the number of the
            // degrees of freedom of the chi-squared distribution of the
            // incoming data. For no integration, k will be 4 (2 powers per
            // poln)
            // Let us now build up a running average of spectrumRMS values
            // (_maxHistory of them)
            
            // if the buffer is not full, compute the new rmsRunAve like this
            // if (!_rmsBuffer.full()) {

            _rms_buffer.push_back(spectrum_rms);
            _rms_run_ave = std::accumulate(_rms_buffer.begin(), _rms_buffer.end(),
                                         0.0)/_rms_buffer.size();
            // The very last time this is done, store a reference value of
            // the mean over the rms; this works as I have just added on the
            // last value two lines above
            if (_rms_buffer.full()) _mean_over_rms = _mean_run_ave / _rms_run_ave;

            // and update the model
            data_model = data_model + mean_minus_minimum * _rms_run_ave;
        }
        
        else {
            // just update the running average with the current last value,
            // and take the oldest off the end. Then add the new
            // value onto the buffer. The idea here is that the new
            // value is not used for the current spectrum, but rather
            // for the one after. The current spectrum is evaluated
            // based on the rms values in the buffer up to that
            // point.
            _rms_run_ave -= (_rms_buffer.front()/_rms_buffer.size());
            _rms_run_ave += (_rms_buffer.back()/_rms_buffer.capacity());

            // In extreme RFI cases, the measured RMS may start growing
            // due to particular RFI signals. The mean over rms ratio
            // should remain approximately constant over the course of the
            // observation. Use this as a safety check, after the mean has
            // been reasonably determined, to set the RMS to a more
            // reliable value :
            if  (_use_mean_over_rms)
                //recover the rms running average
                spectrum_rms = _mean_run_ave / _mean_over_rms;

            _rms_buffer.push_back(spectrum_rms);
            // use the mean running average as a model of the mean of the
            // data; remember that the running average is updated with a
            // mean after channel clipping, below.
            data_model = _mean_run_ave;
        }
        // now use this rms to define a margin of tolerance for bright
        // channels
        float margin = _config.channel_rejection_rms() * _rms_run_ave;
        
        // Now loop around all the channels: if you find a channel where
        // (I - bandpass) - datamodel > margin, then replace it
        unsigned current_channel = 0;
        for(auto & channel_value : sample) {
            if (channel_value - data_model - bandpass[current_channel] > margin) {
                // clipping this channel to values from the last good
                // spectrum
                RfiPolicy::replace_channel( channel_value, 
                                            static_cast<typename std::decay<decltype(channel_value)>::type>(_replacement_sample[current_channel]));
            }
            else{
                unsigned band;
                if(channels_per_band) {
                    band = (int)current_channel / channels_per_band;
                }
                else {
                    band = 0;
                }
                ++good_channels[band];
                spectrum_sum += channel_value;
            }
            ++current_channel;
        }
        // So now we have the mean of the incoming data, in a reliable
        // form after channel clipping
        unsigned total_good_channels=std::accumulate(good_channels.begin(),
                                                     good_channels.end(), 0);
        _fraction_bad_channels += (float)(nchannels - total_good_channels)/nchannels;
        spectrum_sum /= total_good_channels;

        // Check if more than 20% of the channels in each band were
        // bad. If so in more than half of the bands, 4 in this case,
        // keep record. Also, if one band is completely gone, or less
        // than 80% of the total survive, keep record.
        unsigned bad_bands = 0;
        float good_channel_fraction = 0.8;
        for (unsigned b = 0; b < nbands; ++b){
            if (good_channels[b] < good_channel_fraction * channels_per_band) {
                ++bad_bands;
            }
            if (good_channels[b] == 0) {
                bad_bands += nbands / 2;
            }
        }
        if (total_good_channels < good_channel_fraction * nchannels)
            bad_bands += nbands/2;

        // Let us now build up the running average of spectrumSum values
        // (_maxHistory of them) if the buffer is not full, compute the
        // new meanRunAve like this
        

        if (!_mean_buffer.full()) {
            _mean_buffer.push_back(spectrum_sum);
            _mean_run_ave = std::accumulate(_mean_buffer.begin(), _mean_buffer.end(),
                                            0.0)/_mean_buffer.size();
        }
        else {
            //   just update the running average with the new value, and
            //   take the oldest off the end, using the same principle as
            //   with the rms buffer, i.e. do not use the current
            //   measurement for the current spectrum.

            // Note there is a tiny descrepance at the point when the
            // buffer is first full

            _mean_run_ave -= _mean_buffer.front()/_mean_buffer.size();
            _mean_run_ave += _mean_buffer.back()/_mean_buffer.size();
            _mean_buffer.push_back(spectrum_sum);
        }

        // Now we can check if this spectrum has an abnormally high mean
        // compared to the running average

        // Let us define the tolerance first, and remember, we are
        // summing across nBins, hence sqrt(nchannels), strictly only valid
        // for Gaussian stats
        float spectrum_rms_tolerance = _config.spectrum_rejection_rms() * 
            _bandpass.rms()/sqr_nchannels;

        //Now check, if spectrum_sum - model > tolerance, declare this
        //time sample useless, replace its data and take care of the
        //running averages, also cut the first_spectra worth of the
        //first spectra, also cut spectra where badBands >= 4, see
        //above

        unsigned first_spectra = 1000;
        if (_mean_buffer.size() < first_spectra) {
            // clip the sample, but continue to build the stats; this
            // helps the stats converge
            RfiPolicy::replace_sample(sample, _replacement_sample.begin());
        }
        else if (spectrum_sum - _mean_run_ave > spectrum_rms_tolerance || 
                 bad_bands >= nbands / 2 ) {
            // we need to remove this entire spectrum
            RfiPolicy::replace_sample(sample, _replacement_sample.begin());
            // keep a record of bad spectra
            ++_bad_spectra;

            // now remove the last samples from the running average
            // buffers and replace them with the values of the
            // lastgoodspectrum
            _mean_buffer.pop_back();
            _rms_buffer.pop_back();
            _mean_buffer.push_back(_replacement_sample_mean);
            _rms_buffer.push_back(_replacement_sample_rms);
        }
        // else keep a copy of the original spectrum, as it is good, but
        // clip everything down to 3 sigma
        
        else {
            spectrum_sum = 0.0; // 
            unsigned current_channel = 0;
            for(auto const& channel_value : sample) {
                if (channel_value - data_model - bandpass[current_channel]
                    < 3.0 * _rms_run_ave) {
                    _replacement_sample[current_channel] = channel_value;
                }
                spectrum_sum += _replacement_sample[current_channel];
                spectrum_sum_sq += std::pow(_replacement_sample[current_channel],2);
            }
            // and keep the mean and rms values as computed
            _replacement_sample_mean = spectrum_sum / nchannels;
            _replacement_sample_rms = sqrt(spectrum_sum_sq/nchannels - 
                                           std::pow(_replacement_sample_mean,2));
        }
        // Now we have a valid spectrum, either the original or
        // replaced; this spectrum is good, so let us do the final
        // bits of post processing reset the spectrumSum, and SumSq,
        // and flatten subtract the bandpass from the data.
        //
        spectrum_sum = 0.0; 
        spectrum_sum_sq = 0.0; 
        current_channel = 0;
        for(auto & channel_value : sample) {
            channel_value -= (bandpass[current_channel] + data_model);
            spectrum_sum += channel_value;
            spectrum_sum_sq += std::pow(channel_value,2);
        }
        // and normalize: bring to zero mean if zerodm is specified or
        // use the running mean if not
        spectrum_sum /= nchannels; // New meaning of these two variables
        spectrum_rms = sqrt(spectrum_sum_sq/nchannels - std::pow(spectrum_sum,2));

        // Avoid nastiness in those first spectra by avoiding divisions
        // by zero
        if (spectrum_rms == 0.0) spectrum_rms = 1.0;
        current_channel = 0;
        for(auto & channel_value : sample) {
            channel_value -= (float)_zero_dm * spectrum_sum;
            // it may be better to normalize by the running average RMS,
            // given this is a sensitive operation. For example, an
            // artificially low rms may scale things up
            channel_value /= spectrum_rms;
            // make sure this division is not introducing signals that
            // you would have clipped
            if (channel_value > _config.channel_rejection_rms()) 
                channel_value = 0.0;
        }
        // The bandpass is flat and the spectrum clean and normalized,
        // so move to the next spectrum and write out some stats:
        unsigned report_stats_every = 10 * _max_history;
        if (_sample_counter == 0) {
            // calculate fractions
            float fraction_bad_spectra = 100.0 * 
                (float)_bad_spectra / (float)report_stats_every;

            // if the fraction of bad spectra becomes >99%, then empty the
            // circular buffers and go into learning mode again
            if (fraction_bad_spectra > 99.0) {
                _rms_buffer.resize(0);
                _mean_buffer.resize(0);
                PANDA_LOG << "Lost track of the RFI model, retraining.";
            }

            float fraction_bad_channels = 100.0 * 
                _fraction_bad_channels / (float)report_stats_every ;
            PANDA_LOG_DEBUG <<  "RFIstats: mean=" << _mean_run_ave << " rms=" << _rms_run_ave << " bad_spec="
                      << fraction_bad_spectra << " bad_chan" 
                      << fraction_bad_channels ;
            // Reset _bad
            _bad_spectra = 0;
            _fraction_bad_channels = 0.0;

        }
        // and update the model
        _bandpass.set_mean(_mean_run_ave);
        _bandpass.set_rms(_rms_run_ave);
        ++_sample_counter;
        _sample_counter = _sample_counter % report_stats_every;
    }
}

template<typename NumericT>
void Rfim::run(cheetah::data::TimeFrequency<Cpu, NumericT>& data)
{
    Rfim::run_impl<InlineReplacePolicy>(data);
    //Rfim::run_impl<NullPolicy>(data);
}

} // namespace ampp
} // namespace rfim
} // namespace cheetah
} // namespace ska

