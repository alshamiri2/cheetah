/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/ampp/BandPassConfig.h"
#include <boost/units/systems/si/frequency.hpp>


namespace ska {
namespace cheetah {
namespace rfim {
namespace ampp {


BandPassConfig::BandPassConfig()
    : utils::Config("bandpass")
    , _channels(0)
    , _rms(0.0)
{
}

BandPassConfig::~BandPassConfig()
{
}

void BandPassConfig::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("channel_width", boost::program_options::value<double>()->notifier([this](double val) { _channel_width = val * boost::units::si::hertz; }), "the width in Hz of the frequency channel")
    ("freq_start", boost::program_options::value<double>()->default_value(0)->notifier([this](double val) { _start_freq = val * boost::units::si::hertz; }), "the start of the domain in Hz of the bandpass funtion")
    ("number_of_channels", boost::program_options::value<unsigned>(&_channels), "the number of channels in the bandpass model")
    ("p", boost::program_options::value<std::vector<double>>(&_poly), "specify the coefficients of a polynomial to describe the bandpass (order specified corresponds to the coeff number)")
    ("rms", boost::program_options::value<double>(&_rms), "the rms of the bandpass (assumed to be equal across each channel)");
}

std::vector<double> const& BandPassConfig::polynomial_coefficients() const
{
    return _poly;
}

void BandPassConfig::polynomial_coefficients(std::vector<double> params)
{
    _poly = std::move(params);
}

BandPassConfig::FrequencyType BandPassConfig::channel_width() const
{
    return _channel_width;
}

void BandPassConfig::channel_width(FrequencyType freq)
{
    _channel_width= std::move(freq);
}

BandPassConfig::FrequencyType BandPassConfig::start_frequency() const
{
    return _start_freq;
}

void BandPassConfig::start_frequency(FrequencyType freq)
{
    _start_freq = std::move(freq);
}

unsigned BandPassConfig::number_of_channels() const
{
    return _channels;
}

void BandPassConfig::number_of_channels(unsigned channels)
{
    _channels = channels;
}

BandPassConfig::SignalType BandPassConfig::rms() const
{
    return _rms;
}

void BandPassConfig::rms(BandPassConfig::SignalType rms) 
{
    _rms = rms;
}

} // namespace ampp
} // namespace rfim
} // namespace cheetah
} // namespace ska
