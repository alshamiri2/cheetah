/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_AMPP_BANDPASSCONFIG_H
#define SKA_CHEETAH_AMPP_BANDPASSCONFIG_H


#include "cheetah/utils/Config.h"
#include "cheetah/data/Units.h"
#include <boost/units/systems/si/frequency.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <vector>

namespace ska {
namespace cheetah {
namespace rfim {
namespace ampp {

/**
 * @brief
 * 
 * @details
 * 
 */

class BandPassConfig : public utils::Config
{
    public:
        typedef boost::units::quantity<boost::units::si::frequency, double> FrequencyType;
        typedef double SignalType;

    public:
        BandPassConfig();
        ~BandPassConfig();

        std::vector<double> const& polynomial_coefficients() const;
        void polynomial_coefficients(std::vector<double> params);

        FrequencyType channel_width() const;
        void channel_width(FrequencyType);

        FrequencyType start_frequency() const;
        void start_frequency(FrequencyType);

        unsigned number_of_channels() const;
        void number_of_channels(unsigned channels);

        SignalType rms() const;
        void rms(SignalType);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        FrequencyType _start_freq;
        FrequencyType _channel_width;
        unsigned _channels;
        std::vector<double> _poly;
        SignalType _rms;
};


} // namespace ampp
} // namespace rfim
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_AMPP_BANDPASSCONFIG_H 
