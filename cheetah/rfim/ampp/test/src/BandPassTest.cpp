/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/ampp/test/BandPassTest.h"
#include "cheetah/rfim/ampp/BandPass.h"
#include <boost/units/systems/si/prefixes.hpp>


namespace ska {
namespace cheetah {
namespace rfim {
namespace ampp {
namespace test {


BandPassTest::BandPassTest()
    : ::testing::Test()
{
}

BandPassTest::~BandPassTest()
{
}

void BandPassTest::SetUp()
{
}

void BandPassTest::TearDown()
{
}

TEST_F(BandPassTest, test_rms)
{
    ampp::BandPass bp;
    typename ampp::BandPass::AmplitudeType rms(99.0);
    ASSERT_EQ(0.0f, bp.rms());
    bp.set_rms(rms);
    ASSERT_EQ(rms, bp.rms());
}

TEST_F(BandPassTest, test_rebin)
{
    unsigned number_of_bins = 7298;
    typedef ampp::BandPass::FrequencyType FrequencyType;
    utils::BinMap<FrequencyType> map(number_of_bins);
    FrequencyType start(137.304688 * boost::units::si::mega * boost::units::si::hertz);
    FrequencyType width(-0.0007628 * boost::units::si::mega * boost::units::si::hertz);
    map.set_lower_bound(start);
    map.set_bin_width(width);

    ampp::BandPass bp;
    std::vector<double> params = {200.0,  0.0};
    bp.set_params(map, params);
    ASSERT_EQ(map.number_of_bins(), bp.current_set().size());
    
    float rms = 44.3413175435;
    bp.set_rms(rms);
    float mean=91.1;
    bp.set_mean(mean);

    BandPass::AmplitudeType const& a = bp.current_set()[0];
    BandPass::AmplitudeType const& b = bp.current_set()[1];
    ASSERT_NE(0.0f, a);
    ASSERT_NE(0.0f, b);

    {  // Use Case:
        // rebin to twice as many bins over the same range
        utils::BinMap<FrequencyType> map(number_of_bins*2);
        map.set_lower_bound(start);
        map.set_bin_width(width/2.0);
        bp.rebin(map);

        ASSERT_DOUBLE_EQ( mean , 2.0 * bp.mean() );
        ASSERT_FLOAT_EQ( rms * std::sqrt(2.0) , 2.0 * bp.rms() );
        ASSERT_GE( b/2.0f , bp.current_set()[0] ) << "a=" << a << " b=" << b << " b/2=" << b/2.0;
    }
    {  // Use Case:
        // rebin to half as many bins over the same range
        utils::BinMap<FrequencyType> map(number_of_bins/2);
        map.set_lower_bound(start);
        map.set_bin_width(width*2.0);
        bp.rebin(map);
        ASSERT_DOUBLE_EQ( 2.0 * mean , bp.mean() );
        ASSERT_FLOAT_EQ( 2.0 * rms / std::sqrt(2.0) , bp.rms() );
        ASSERT_GE( b*2.0f , bp.current_set()[0] ) << "a=" << a << " b=" << b << " b/2=" << b/2.0;
    }
}

TEST_F(BandPassTest, test_set_mean)
{
    unsigned number_of_bins = 1;
    typedef ampp::BandPass::FrequencyType FrequencyType;
    utils::BinMap<FrequencyType> map(number_of_bins);
    FrequencyType start(137.304688 * boost::units::si::mega * boost::units::si::hertz);
    FrequencyType width(-0.0007628 * boost::units::si::mega * boost::units::si::hertz);
    map.set_lower_bound(start);
    map.set_bin_width(width);

    ampp::BandPass bp;
    std::vector<double> params = {4460.84130843,  -24.8135957376};
    bp.set_params(map, params);
    ASSERT_EQ(map.number_of_bins(), bp.current_set().size());

    BandPass::AmplitudeType mean=bp.mean();

    // Use Case:
    // set mean on the primary map
    // expect values to be shifted
    BandPass::AmplitudeType half_mean = mean/2.0;
    bp.set_mean(half_mean);
    ASSERT_FLOAT_EQ( half_mean , bp.mean());

    // TODO test if rebineed maps also scale

}

} // namespace test
} // namespace ampp
} // namespace rfim
} // namespace cheetah
} // namespace ska
