/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/test_utils/RfimTester.h"
#include "cheetah/rfim/Metrics.h"
#include "cheetah/generators/GaussianNoise.h"
#include "cheetah/generators/GaussianNoiseConfig.h"
#include "cheetah/generators/RfiScenario.h"
#include "cheetah/data/TimeFrequency.h"
#include <boost/units/systems/si/frequency.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <limits>
#include <algorithm>
#include <functional>
#include <iostream>
#include <string>


namespace ska {
namespace cheetah {
namespace rfim {
namespace test {


template <typename TestTraits>
RfimTester<TestTraits>::RfimTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
RfimTester<TestTraits>::~RfimTester()
{
}

template<typename TestTraits>
void RfimTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void RfimTester<TestTraits>::TearDown()
{
}

template<typename TestTraits>
void RfimTester<TestTraits>::verify_equal(typename TestTraits::DataType const& d1, typename TestTraits::DataType const& d2)
{
    ASSERT_EQ(d1.number_of_samples(), d2.number_of_samples());
    ASSERT_EQ(d1.number_of_channels(), d2.number_of_channels());
    auto it = d2.begin();
    for(auto const val : d1) {
        ASSERT_DOUBLE_EQ(val, *it++);
    }
}

template<typename DataType>
static
std::shared_ptr<DataType> generate_time_freq_data(unsigned number_of_samples, unsigned number_of_channels)
{
    typedef typename DataType::FrequencyType FrequencyType;

    std::shared_ptr<DataType> data = std::make_shared<DataType>(number_of_samples, number_of_channels);
    FrequencyType delta( 1.0 * boost::units::si::mega * boost::units::si::hertz);
    FrequencyType start( 100.0 * boost::units::si::mega * boost::units::si::hertz);

    data->set_channel_frequencies_const_width(start, delta);

    // generate a spectrum
    return data;
}


template <typename TypeParam, typename DeviceType>
void test_rfi_algorithm(
    std::function<void (
        typename TypeParam::DataType::TimeFrequencyFlagsType const &,
        typename TypeParam::DataType::TimeFrequencyFlagsType const &)> assertion,
        DeviceType &device, typename TypeParam::DataType &data)
{
    using DataType = typename TypeParam::DataType;
    using TimeFrequencyFlags = typename DataType::TimeFrequencyFlagsType;

    TimeFrequencyFlags expected_flags(*data.get_flags());
    auto current_flags = data.get_flags();
    std::fill(current_flags->begin(), current_flags->end(), false);

    TypeParam::apply_algorithm(device, data);

    assertion(expected_flags, *current_flags);
}

template<int Num, typename TypeParam, typename DeviceType, typename Enable = void>
struct RfiScenarioLauncher {
    typedef typename TypeParam::DataType DataType;
    inline static
    void exec(DataType&, DeviceType&) {
        //std::string const test_name("RfiScenario<" + std::to_string(Num) + ">");
        //std::cout << "test does not exist: " << test_name << std::endl;
    }
};

template<int Num, typename TypeParam, typename DeviceType>
struct RfiScenarioLauncher<Num, TypeParam, DeviceType, typename std::enable_if<std::is_constructible<generators::RfiScenario<Num, TypeParam>>::value>::type >
{
    typedef typename TypeParam::DataType DataType;
    typedef typename DataType::TimeFrequencyFlagsType TimeFrequencyFlags;
    typedef typename DataType::DataType NumRepType;
    // aply the N'th pre-defined Rfi Scenario to the data
    inline static
    void exec(DataType& data, DeviceType& device) {
        std::string const test_name("RfiScenario<" + std::to_string(Num) + ">");
        std::cout << "running test: " << test_name << std::endl;
        auto copy = data;
        cheetah::generators::RfiScenario<Num, NumRepType> scenario;
        scenario.next(copy);
        test_rfi_algorithm<TypeParam>(
            [test_name] (TimeFrequencyFlags const &expected, TimeFrequencyFlags const &given)
            {
                SCOPED_TRACE( test_name );
                rfim::Metrics m(expected, given);
                std::cout << test_name << " metrics\n"
                          << "\t" << "correct=" << m.num_correct() << " of " << m.num_rfi() << " (" << m.correct_percentage() << "%)\n"
                          << "\t" << "false +ve=" << m.num_false_positives() << " (" << m.false_positives_percentage() << "%)\n"
                          << "\t" << "false -ve=" << m.num_false_negatives() << " (" << m.false_negatives_percentage() << "%)\n"
                          ;
                EXPECT_EQ(std::size_t(0), m.num_false_positives());
                EXPECT_LT(m.false_negatives_percentage(), 50.0); // rather arbitary 50% detection rate
            }, device, data);
        RfiScenarioLauncher<Num+1, TypeParam, DeviceType>::exec(data, device);
    }

};

ALGORITHM_TYPED_TEST_P(RfimTester, test_single_channel_zero)
{
    using DataType = typename TypeParam::DataType;
    using TimeFrequencyFlags = typename DataType::TimeFrequencyFlagsType;
    using NumericalRep = typename DataType::DataType;
    typename DataType::FrequencyType delta( 1.0 * boost::units::si::mega * boost::units::si::hertz);
    typename DataType::FrequencyType start( 100.0 * boost::units::si::mega * boost::units::si::hertz);

    generators::GaussianNoiseConfig config;
    generators::GaussianNoise<NumericalRep> noise(config);
    std::shared_ptr<DataType> time_frequency_data_ref(new DataType(data::DimensionSize<data::Time>(100), data::DimensionSize<data::Frequency>(100)));
    time_frequency_data_ref->set_channel_frequencies_const_width(start, delta);

    auto flags = time_frequency_data_ref->get_flags();
    std::fill(flags->begin(), flags->end(), false);
    auto ch_flags = flags->channel(50);
    std::fill(ch_flags.begin(), ch_flags.end(), false);
    noise.next(*time_frequency_data_ref);

    test_rfi_algorithm<TypeParam>(
        [] (TimeFrequencyFlags const &expected, TimeFrequencyFlags const &given)
        {
            ASSERT_TRUE(expected == given);
        }, device, *time_frequency_data_ref);
}

ALGORITHM_TYPED_TEST_P(RfimTester, gaussian_noise_wth_rfi)
{
    // Generates a series of tests each with a gaussian noise with the RfiScenario<n> imposed on top
    // n.b RfiScenario<0> is no RFI - i.e just gaussian noise.
    typedef typename TypeParam::DataType DataType;
    using TimeFrequencyFlags = typename DataType::TimeFrequencyFlagsType;
    using NumericalRep = typename DataType::DataType;
    typename DataType::FrequencyType delta( 1.0 * boost::units::si::mega * boost::units::si::hertz);
    typename DataType::FrequencyType start( 100.0 * boost::units::si::mega * boost::units::si::hertz);

    generators::GaussianNoiseConfig config;
    generators::GaussianNoise<NumericalRep> noise(config);
    std::shared_ptr<DataType> time_frequency_data(new DataType(data::DimensionSize<data::Time>(1024), data::DimensionSize<data::Frequency>(100))); // at least one channel
    time_frequency_data->set_channel_frequencies_const_width(start, delta);
    auto flags = time_frequency_data->get_flags();
    std::fill(flags->begin(), flags->end(), false);

    // set up a gaussian noise signal
    noise.next(*time_frequency_data);

    // execute all RFI Scenarios
    //typedef typename std::decay<decltype(device)>::type::element_type DeviceType;
    typedef decltype(device) DeviceType;
    RfiScenarioLauncher<0, TypeParam, DeviceType>::exec(*time_frequency_data, device);
}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_CASE_P(RfimTester, gaussian_noise_wth_rfi, test_single_channel_zero);

} // namespace test
} // namespace rfim
} // namespace cheetah
} // namespace ska
