#ifndef SKA_CHEETAH_RFIM_RFIMBLOCK_H
#define SKA_CHEETAH_RFIM_RFIMBLOCK_H


namespace ska {
namespace cheetah {
namespace rfim {

/**
 * @brief
 *   Wrap a Rfim flaggeing style detector and adjust the data according to the flags and a specific policy
 *
 * @details
 *
 */

template<class RfimDetector, class RfimPolicy>
class RfimBlock
{
    public:
        typedef typename RfimDetector::Architecture Architecture;

    public:
        RfimBlock(RfimDetector&&);
        ~RfimBlock();

        template<typename ResourceType, typename DataType>
        //typename RfimDetector::DataType& operator()(ResourceType&&, std::shared_ptr<typename RfimDetector::DataType> data );
        DataType& operator()(ResourceType&&, std::shared_ptr<DataType> data );


    private:
        RfimDetector _detector;
        RfimPolicy _policy;
};


} // namespace rfim
} // namespace cheetah
} // namespace ska
#include "cheetah/rfim/detail/RfimBlock.cpp"

#endif // SKA_CHEETAH_RFIM_RFIMBLOCK_H
