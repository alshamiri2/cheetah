#ifndef SKA_CHEETAH_RFIM_RFIMPOLICYLASTUNFLAGGED_H
#define SKA_CHEETAH_RFIM_RFIMPOLICYLASTUNFLAGGED_H

#include "cheetah/rfim/RfimTypes.h"
#include "cheetah/rfim/FlaggedData.h"
#include <memory>

namespace ska {
namespace cheetah {
namespace rfim {

/**
 * @brief
 *
 * @details
 *
 */

class RfimPolicyLastUnflagged
{
    public:
        RfimPolicyLastUnflagged();
        ~RfimPolicyLastUnflagged();

        template<typename DataType>
        DataType& operator()(FlaggedData const& flags, DataType& data);

    private:
};


} // namespace rfim
} // namespace cheetah
} // namespace ska
#include "detail/RfimPolicyLastUnflagged.cpp"

#endif // SKA_CHEETAH_RFIM_RFIMPOLICYLASTUNFLAGGED_H
