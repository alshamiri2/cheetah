#include "cheetah/rfim/RfimPolicyLastUnflagged.h"


namespace ska {
namespace cheetah {
namespace rfim {

template<typename DataType>
DataType& RfimPolicyLastUnflagged::operator()(FlaggedData const& flags, DataType& data)
{
    (void) flags;
    return data;
}

} // namespace rfim
} // namespace cheetah
} // namespace ska
