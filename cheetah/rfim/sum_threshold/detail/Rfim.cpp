#include "cheetah/rfim/sum_threshold/Rfim.h"
#include "panda/Buffer.h"
#include "panda/Log.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#pragma GCC diagnostic pop
#include <cmath>


namespace ska {
namespace cheetah {
namespace rfim {
namespace sum_threshold {

namespace {
typedef panda::Buffer<bool> FlagType;

inline float calcThresholdI(float threshold1, unsigned window, float p)
{
    if (p <= 0.0f) {
        p = 1.5f; // according to Andre's RFI paper, this is a good default value
    }
    return (float) (threshold1 * std::pow(p, std::log2(window)) / window);
}

template<typename IteratorType>
static void calculateNormalStatistics(
    IteratorType&& begin, IteratorType const&& end, float& mean, float& median, float& std_dev)
{
    boost::accumulators::accumulator_set<
        double, boost::accumulators::stats<boost::accumulators::tag::variance,
        boost::accumulators::tag::median>> acc;
    for_each(begin, end, std::bind<void>(std::ref(acc), std::placeholders::_1));

    mean = boost::accumulators::mean(acc);
    std_dev = sqrtf(boost::accumulators::variance(acc));
    median = boost::accumulators::median(acc);
}

template<typename IteratorType>
static void calculateStatistics(IteratorType&& begin, IteratorType const&& end, float& mean, float& median, float& std_dev) {
    // In Rob's code, there is also an option for calculateWinsorizedStatistics. I left it out for now.
    calculateNormalStatistics(std::forward<IteratorType>(begin), std::forward<IteratorType const>(end),  mean, median, std_dev);
}



template<typename DataType>
static unsigned sumThreshold2DHorizontal(DataType const & powers, FlagType &flags, const unsigned window, const float threshold) {
    unsigned extra_flagged = 0;

    for(unsigned channel=0; channel<powers.number_of_channels(); ++channel) {
        for (unsigned base = 0; base + window < powers.number_of_spectra(); ++base) {
            float sum = 0.0f;

            for (unsigned time = base; time < base + window; time++) {
                assert(flags.size() > channel*powers.number_of_spectra() + time);
                if (flags.data()[channel*powers.number_of_spectra() + time]) { // If it was flagged in a previous iteration, replace sample with current threshold
                    sum += threshold;
                } else {
                    sum += powers.spectrum(time)[channel];
                }
            }

            if (sum >= window * threshold) {
                // flag all samples in the sequence!
                for (unsigned time = base; time < base + window; time++) {
                    if(!flags.data()[channel*powers.number_of_spectra() + time]) {
                        extra_flagged++;
                        flags.data()[channel*powers.number_of_spectra() + time] = true;
                    }
                }
            }
        }
    }

    return extra_flagged;
}

template<typename DataType>
static unsigned sumThreshold2DVertical(DataType const & powers
                                      , FlagType &flags
                                      , const unsigned window
                                      , const float threshold)
{
    unsigned extra_flagged = 0;

    for(unsigned channel=0; channel<powers.number_of_channels(); ++channel) {
        for (unsigned base = 0; base + window < powers.number_of_spectra(); base++) {
            float sum = 0.0f;

            for (unsigned time = base; time < base + window; time++) {
                if(flags.data()[channel*powers.number_of_spectra() + time]) { // If it was flagged in a previous iteration, replace sample with current threshold
                    sum += threshold;
                } else {
                    sum += powers.spectrum(time)[channel];
                }
            }

            if (sum >= window * threshold) {
                // flag all samples in the sequence!
                for (unsigned time = base; time < base + window; time++) {
                    if(!flags.data()[channel*powers.number_of_spectra() + time]) {
                        extra_flagged++;
                        flags.data()[channel*powers.number_of_spectra() + time] = true;
                    }
                }
            }
        }
    }

    return extra_flagged;
}
} // namespace

template<typename DataType>
FlaggedData Rfim::operator()(panda::PoolResource<Cpu>&, std::shared_ptr<DataType> data)
{
    FlaggedData flagged_data;

    float mean, std_dev, median;
    FlagType flags(data->number_of_spectra() * data->number_of_channels()); //TODO use to flagged_data
    calculateStatistics(data->begin(), data->end(), mean, median, std_dev);
    float factor;
    if (std_dev == 0.0f) {
      factor = _config.base_sensitivity();
    } else {
      factor = std_dev * _config.base_sensitivity();
    }

    unsigned extra_flagged = 0;

    assert(data);
    for (unsigned window : _config.thresholding_data_sizes()) {
      float thresholdI = median + calcThresholdI(_config.its_cutoff_threshold(), window, 1.5f) * factor;
      extra_flagged += sumThreshold2DHorizontal(*data, flags, window, thresholdI);
      extra_flagged += sumThreshold2DVertical(*data, flags, window, thresholdI);
    }

    // Populate the TimeFrequencyFlags data structure
    for ( unsigned int channel = 0; channel < data->number_of_channels(); ++channel ) {
        auto channel_iterator = (data->get_flags())->channel(channel);
        unsigned int sample_number = 0;

        for ( auto sample = channel_iterator.begin(); sample != channel_iterator.end(); ++sample ) {
            *sample = static_cast<data::FlagsType>(flags.data()[(channel * data->number_of_spectra()) + sample_number]);
            ++sample_number;
        }
    }

    //return *data;
    return flagged_data;
}


} // namespace sum_threshold
} // namespace rfim
} // namespace cheetah
} // namespace ska
