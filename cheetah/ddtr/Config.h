/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_CONFIG_H
#define SKA_CHEETAH_DDTR_CONFIG_H
#include "cheetah/ddtr/DedispersionConfig.h"
#include "cheetah/ddtr/fpga/Config.h"
#include "cheetah/ddtr/cpu/Config.h"
#include "cheetah/utils/Config.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/data/DmConstant.h"
#include "panda/PoolSelector.h"
#include <cstdlib>
#include <vector>

namespace ska {
namespace cheetah {
namespace ddtr {

/**
 * @brief configuration parameters for the DDTR module
 *
 */

class Config : public cheetah::utils::Config
{
    public:
        typedef data::DedispersionMeasureType<float> Dm;
        typedef boost::units::quantity<data::dm_constant::s_mhz::Unit, double> DmConstantType;
        
    public:
        Config();

        ~Config();

         /**
         * @brief get DM constant
         * @return multiplicative factor giving DM delay in seconds for MHZ frequencies
         */
         DmConstantType dm_constant() const;

         /**
         * @brief set DM constant
         * @return multiplicative factor giving DM delay in seconds for MHZ frequencies
         */
         void dm_constant(Config::DmConstantType dm_const);

         /**
         * @brief list of DM trials
         */
        std::vector<Dm> dm_trials() const;

         /**
         * @brief get range of DM values
         */
        void add_dm_range(Dm start, Dm end, Dm step);

         /**
         * @brief return the fpga algorithm configuration parameters
         */
        fpga::Config const& fpga_algo_config() const;
        fpga::Config& fpga_algo_config();

         /**
         * @brief return the cpu algorithm configuration parameters
         */
        cpu::Config const& cpu_algo_config() const;
        cpu::Config& cpu_algo_config();

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        DmConstantType _dm_constant;
        fpga::Config _fpga_config;
        cpu::Config _cpu_config;
};

typedef panda::PoolSelector<typename Config::PoolManagerType, Config> ConfigType;

} // namespace ddtr
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_DDTR_CONFIG_H
