SUBPACKAGE(cpu)
SUBPACKAGE(fpga)

set(module_ddtr_lib_src_cpu
    src/Config.cpp
    src/DedispersionConfig.cpp
    ${lib_src_cpu}
    PARENT_SCOPE
)

TEST_UTILS()
add_subdirectory(test)

