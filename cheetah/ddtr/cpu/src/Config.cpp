/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/cpu/Config.h"


namespace ska {
namespace cheetah {
namespace ddtr {
namespace cpu {


Config::Config()
    : utils::Config("cpu")
    , _active(false)
    , _dedispersion_samples(1<<15)
{
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("active", boost::program_options::value<bool>(&_active)->default_value(_active), "use this algorithm for dedispersion")
    ("dedispersion_samples", boost::program_options::value<std::size_t>(&_dedispersion_samples)->
        default_value(_dedispersion_samples), "the desired number of samples to process in each call to the dedisperser");
}

bool Config::active() const
{
    return _active;
}

void Config::active(bool state)
{
    _active = state;
}

std::size_t Config::dedispersion_samples() const
{
    return _dedispersion_samples;
}

void Config::dedispersion_samples(std::size_t n)
{
    _dedispersion_samples = n;
}

} // namespace cpu
} // namespace ddtr
} // namespace cheetah
} // namespace ska
