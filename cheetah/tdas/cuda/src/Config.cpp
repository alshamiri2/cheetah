/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/tdas/cuda/Config.h"


namespace ska {
namespace cheetah {
namespace tdas {
namespace cuda {

Config::Config()
    : utils::Config("cuda")
#ifdef ENABLE_CUDA
    , _active(true)
#else
    , _active(false)
#endif
{
    add(_dred_config);
    add(_brdz_config);
    add(_tdrt_config);
    add(_tdao_config);
    add(_pwft_config);
    add(_hrms_config);
    add(_fft_config);
}

Config::~Config()
{
}

tdrt::Config const& Config::tdrt_config() const
{
    return _tdrt_config;
}

tdrt::Config& Config::tdrt_config()
{
    return _tdrt_config;
}

tdao::Config const& Config::tdao_config() const
{
    return _tdao_config;
}

tdao::Config& Config::tdao_config()
{
    return _tdao_config;
}

pwft::Config const& Config::pwft_config() const
{
    return _pwft_config;
}

pwft::Config& Config::pwft_config()
{
    return _pwft_config;
}

hrms::Config const& Config::hrms_config() const
{
    return _hrms_config;
}

hrms::Config& Config::hrms_config()
{
    return _hrms_config;
}

fft::Config const& Config::fft_config() const
{
    return _fft_config;
}

fft::Config& Config::fft_config()
{
    return _fft_config;
}

brdz::Config const& Config::brdz_config() const
{
    return _brdz_config;
}

brdz::Config& Config::brdz_config()
{
    return _brdz_config;
}

dred::Config const& Config::dred_config() const
{
    return _dred_config;
}

dred::Config& Config::dred_config()
{
    return _dred_config;
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("active", boost::program_options::value<bool>(&_active), "use the cuda based algorithm for time domain search");
}

bool Config::active() const
{
    return _active;
}

} // namespace cuda
} // namespace tdas
} // namespace cheetah
} // namespace ska
