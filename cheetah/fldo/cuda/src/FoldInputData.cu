/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/cuda/detail/FldoUtils.h"
//#include "cheetah/cuda_utils/cuda_errorhandling.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
namespace util {

/**
 * void fold_input_data(float *d_folded, float *d_weight, int ncand, int * isubint, int *nbins, int * xfactor)
 *
 * @brief
 * Sets the number of threads and shared memory size and call the GPU
 * kernel to execute the folding algorithm.
 *
 * @param d_folded      pointer to the device memory with folded data
 * @param d_weight      pointer to the device memory with data weights
 * @param ncand         the candidate index
 * @param isubint       the subint index
 * @param rebin         the rebin structure
 * @param nbins         the array with the phases bins of the candidates
 * @param xfactor       the array with the period multiplicative factor
 *                      of the candidates
 */
void fold_input_data(cudaDeviceProp gpu_properties, float *d_folded, float *d_weight, int *nbins, int * xfactor,
                     CandidateRebin const &rebin, int ncand, int isubint, int nchannels, int nsubbands,
                     uint64_t nsamp_subslot, int default_max_phase, double tobs, bool enable_split,
                     cudaStream_t exec_stream)
{
    dim3 threadsPerBlock;       // number of kernel threads per block
    dim3 blocksPerGrid;         // number of kernel blocks for grid

    // get some properties of the device. These info are used during the
    // configuration of GPU kernel launch.
    size_t warp_size = gpu_properties.warpSize;
    size_t max_threads_per_block = gpu_properties.maxThreadsPerBlock;
    size_t shared_mem_per_block = gpu_properties.sharedMemPerBlock;
    PANDA_LOG_DEBUG << "shared_mem_per_block: "
                    << shared_mem_per_block
                    << " warp_size: "
                    << warp_size;

    //threads in x represents the steps in time so threadsPerBlock.x < nsamp_subslot
    threadsPerBlock.x = 32;
    if (threadsPerBlock.x > (size_t)rebin.pitch_dim) {
        threadsPerBlock.x = rebin.pitch_dim;  // should not happen!!
    }
    //threads in y represents the steps in freqs inside each band. So
    //threadsPerBlock.y <= number of chans/band
    threadsPerBlock.y = 8;          // good value (32 x 8 = 256 threads/block)
    if (nchannels/nsubbands < 8) {
        threadsPerBlock.y = nchannels/nsubbands;
    }
    blocksPerGrid.x = nsubbands;
    blocksPerGrid.y = 1;
    // calculate the max number of threads in x
    size_t max_threads_x = max_threads_per_block/threadsPerBlock.y;

    if (threadsPerBlock.x > max_threads_x) {
        threadsPerBlock.x = max_threads_x;
    }
    // take into account that the number of nsamp in each integration
    // is not a multiple of the thread dimension in x
    if ((rebin.pitch_dim % threadsPerBlock.x) != 0) {
        if (threadsPerBlock.x * threadsPerBlock.y < max_threads_per_block) {
            threadsPerBlock.x += 1;
        } else {
            // too much threads for SM: can't run kernel
            std::stringstream error_msg;
            error_msg << "folding kernel: Invalid kernel parameters: threads for each SM: "
                      << threadsPerBlock.x * threadsPerBlock.y
                      << "(max: "
                      << max_threads_per_block
                      << ")";
            PANDA_LOG_ERROR << error_msg.str();
            throw panda::Error(error_msg.str());
        }
    }
    int warp_count = threadsPerBlock.x * threadsPerBlock.y/warp_size;
    PANDA_LOG_DEBUG << "warp_count : " << warp_count;
    if (((threadsPerBlock.x * threadsPerBlock.y) % warp_size) != 0) {
        warp_count += 1;
    }
    int max_phase = default_max_phase; //initiliaze the default value of phase bins
    //adjust the max_phase value on the nbins of the current candidate
    if (nbins[ncand] < 15) {
        max_phase = 16;
    } else if (nbins[ncand] < 31) {
        max_phase = 32;
    } else if (nbins[ncand] < 63) {
        max_phase = 64;
    }
    // configure shared memory size
    // OSS: factor 2 because we store in shared memory the partial folded data
    // and phases weights
    size_t shared_memory_size = max_phase * warp_count * sizeof(float) * 2;
    if (shared_memory_size > shared_mem_per_block) {
        std::stringstream err_msg;
        err_msg << "Shared memory requested size is too big (requested: "
                << shared_memory_size
                << " available: "
                << shared_mem_per_block
                << ")";
        PANDA_LOG_ERROR << err_msg.str();
        throw panda::Error(err_msg.str());
    }
    int threadblock_memory = max_phase * warp_count;
    //the folding kernel is executed on a different stream for group of candidate.
    //This kernel starts only when the data rebinning of the input data
    //(with the rebin of the current candidate) has ended
    if (enable_split == true) {
        folding_worker<<< blocksPerGrid, threadsPerBlock, shared_memory_size, exec_stream >>>
        (rebin.d_out,
         d_folded,
         d_weight,
         rebin.pitch_dim,
         (int)nsamp_subslot,
         ncand,
         xfactor[ncand],
         rebin.rebin,
         max_phase,
         warp_count,
         tobs, default_max_phase, isubint,
         threadblock_memory);
    } else {
        folding_worker_nosplit<<< blocksPerGrid, threadsPerBlock, shared_memory_size, exec_stream >>>
        (rebin.d_out,
         d_folded,
         d_weight,
         rebin.pitch_dim,
         (int)nsamp_subslot,
         ncand,
         xfactor[ncand],
         rebin.rebin,
         max_phase,
         warp_count,
         tobs,
         default_max_phase,
         isubint,
         threadblock_memory);
    }
    CUDA_ERROR_CHECK(cudaGetLastError());
}

} // utils
} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska
