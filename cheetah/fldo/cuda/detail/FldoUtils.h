/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_TEST_UTILS_FLDOUTILS_H
#define SKA_CHEETAH_FLDO_TEST_UTILS_FLDOUTILS_H

#ifdef ENABLE_CUDA

#include "cheetah/fldo/Types.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "panda/Error.h"
#include "panda/Log.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
namespace util {

/**
 * class CandidateRebin
 *
 * @brief
 */
class CandidateRebin
{
    public:
        uint64_t msize;     // memory size in bytes
        int rebin;          // rebinning factor: 1, 2, 4, 8,.. (power of 2)
        int first;          // index of the first candidate with current rebinning factor
        int last;           // index of the last candidate with current rebinning factor
        int pitch_dim;      // pitch dim to allocate device memory
        float *d_out;       // pointer to memory with transposed rebinned matrix
        cudaStream_t stream;// cuda stream associated to the current rebin factor
        cudaEvent_t event;  // cuda event associated to the current stream
    public:
        CandidateRebin()
        {
            first         = -1;
            last          = -1;
            pitch_dim     = -1;
            msize         = 0;
            d_out = NULL;
            //associate a CUDA event and a CUDA steam to each rebin object
            CUDA_ERROR_CHECK(cudaEventCreateWithFlags(&event, cudaEventDisableTiming));
            CUDA_ERROR_CHECK(cudaStreamCreate(&stream));
        }
        ~CandidateRebin() {
            CUDA_ERROR_CHECK(cudaEventDestroy(event));
            CUDA_ERROR_CHECK(cudaStreamDestroy(stream));
            if (d_out) {
                cudaFree(d_out);
            }
        };
};

/*
 * class GpuEvent
 *
 * Simple interface for start/stop timing events.
 */
class GpuEvent
{
    public:
        GpuEvent()
        {
            CUDA_ERROR_CHECK(cudaEventCreate(&_start_kernel));
            CUDA_ERROR_CHECK(cudaEventCreate(&_stop_kernel));
        }
        ~GpuEvent()
        {
            CUDA_ERROR_CHECK(cudaEventDestroy(_start_kernel));
            CUDA_ERROR_CHECK(cudaEventDestroy(_stop_kernel));
        }
        /**
         * void gpu_event_start()
         * @brief Records the start event
         */
        void gpu_event_start()
        {
             CUDA_ERROR_CHECK(cudaEventRecord(_start_kernel, 0));
        }
        /**
         * void gpu_event_stop()
         * @brief Records the stop event
         */
        void gpu_event_stop()
        {
             CUDA_ERROR_CHECK(cudaEventRecord(_stop_kernel, 0));
        }
        /**
         * float gpu_elapsed_time()
         * @brief Returns the elapsed time in ms. between start/stop events
         */
        float gpu_elapsed_time()
        {
            float gpu_time;
            CUDA_ERROR_CHECK(cudaEventSynchronize(_stop_kernel));
            cudaEventElapsedTime(&gpu_time, _start_kernel, _stop_kernel);
            return gpu_time;
        }

    private:
        cudaEvent_t _start_kernel;      // event to record kernel start
        cudaEvent_t _stop_kernel;       // event to record kernel stop
};

/*
 * class GpuStream
 *
 * Simple interface to handle kernel streams different from the default
 * one.
 */
class GpuStream
{
    public:
        GpuStream()
        {
            CUDA_ERROR_CHECK(cudaStreamCreate(&_stream));
        }

        ~GpuStream()
        {
            CUDA_ERROR_CHECK(cudaStreamDestroy(_stream));
        }

        /**
         * cudaStream_t const &stream() const
         *
         * @brief Return the created stream
         */
        cudaStream_t const &stream() const
        {
             return _stream;
        }
    private:
        cudaStream_t _stream;   // kernel stream different from the default
};

typedef data::Candidate<Cpu, float> CandidateType;
typedef data::TimeType TimeType;

void load_constant_data(double *delta_freq, double *nu, double *nudot, float *dm,
                        int *nbins, int nchannels, int nchan_per_subband, size_t nsubint, double tsamp,
                        int ncandidates);
void rebin_input_data(int start, int current, int nchannels, std::vector<CandidateRebin> const &rebin);
void fold_input_data(cudaDeviceProp properties, float *d_folded, float *d_weight, int *nbins, int * xfactor,
                     CandidateRebin const &rebin, int ncand, int isubint, int nchannels,
                     int nsubbands, uint64_t nsamp_subslot, int default_max_phase, double tobs,
                     bool enable_split, cudaStream_t const exec_stream);
void statistics_float(float *raw_in, int N, float *mean, float *rms, cudaStream_t stream);
//void statistics(unsigned char *raw_in, int N, float *mean, float *rms, cudaStream_t stream);
void corner_turner_and_rebin(cudaDeviceProp properties, int first_bin_idx, uint64_t nsamp_per_subslot,
                             size_t nchannels, std::vector<CandidateRebin> &rebin, unsigned char *d_in);
void build_scrunched_profiles(size_t ncandidates, size_t max_phases, size_t nsubbands, size_t nsubint, float mean,
                              float *d_folded, float *d_weight, float *d_outfprof, float* d_outprof,
                              std::vector<util::GpuStream>&exec_stream);



} // util
} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska

#endif //ENABLE_CUDA

#endif // SKA_CHEETAH_FLDO_TEST_UTILS_FLDOUTIL_H
